# -*- coding: utf-8 -*-

__author__ = 'Pero'

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tpo_projekt.settings')

import django
import datetime
from django.utils import timezone

django.setup()

from estudis.models import *
import estudis.utility as util


def add_drzave(sifrant_drzav):
    with open(sifrant_drzav) as f:
        for line in f:
            line = unicode(line, 'utf-8')
            split = line.split(';')
            id_drzave = int(split[1])
            naziv_drzave = split[2]
            d = Drzava(iddrzava=id_drzave, naziv=naziv_drzave)
            d.save()


def add_obcine(seznam_obcin):
    obcina_id = 1
    with open(seznam_obcin) as f:
        for line in f:
            line = unicode(line, 'utf-8')
            split = line.split(';')
            obcina_naziv = split[0]
            o = Obcina(idobcina=obcina_id, naziv=obcina_naziv)
            o.save()
            obcina_id += 1


def add_poste(seznam_poste):
    with open(seznam_poste) as f:
        for line in f:
            line = unicode(line, 'utf-8')
            split = line.split(';')
            postna_stevilka = split[1]
            naziv_poste = split[2] + ' ' + split[3]
            p = Posta(postna_stevilka=postna_stevilka, naziv=naziv_poste)
            p.save()


# obstajata samo 2 načina študija, funkcija zgenerira oba
def add_nacin_studija():
    n1 = NacinStudija(idnacin_studija=1, naziv='redni')
    n3 = NacinStudija(idnacin_studija=3, naziv='izredni')
    n1.save()
    n3.save()


# zgenerira vrste vpisa
def add_vrsta_vpisa():
    v1 = VrstaVpisa(idvrsta_vpisa=01, naziv='Prvi vpis/dodatni letnik')
    v2 = VrstaVpisa(idvrsta_vpisa=02, naziv='Ponavljanje letnik')
    v3 = VrstaVpisa(idvrsta_vpisa=03, naziv='Nadaljevanje letnika')
    v4 = VrstaVpisa(idvrsta_vpisa=04, naziv='Podaljšanje statusa študenta')
    v5 = VrstaVpisa(idvrsta_vpisa=05, naziv='Vpis po merilih za prehode v višji letnik')
    v6 = VrstaVpisa(idvrsta_vpisa=06, naziv='Vpis v semester skupnega št. programa')
    v7 = VrstaVpisa(idvrsta_vpisa=07, naziv='Vpis po merilih za prehode v isti letnik')
    v98 = VrstaVpisa(idvrsta_vpisa=98, naziv='Vpis za zaključek')

    v1.save()
    v2.save()
    v3.save()
    v4.save()
    v5.save()
    v6.save()
    v7.save()
    v98.save()


#zgenerira oblike študija
def add_oblika_studija():
    o1 = OblikaStudija(idoblika_studija=1, naziv='Na lokaciji')
    o2 = OblikaStudija(idoblika_studija=2, naziv='Na daljavo')
    o3 = OblikaStudija(idoblika_studija=3, naziv='E-študij')
    o1.save()
    o2.save()
    o3.save()


def add_vpisani_predmeti(idvpisani_predmeti, sifra_predmeta, vpis_stevilka_vpisa):
    v = VpisaniPredmeti.objects.get_or_create(idvpisani_predmeti=idvpisani_predmeti, sifra_predmeta=sifra_predmeta,
                                              vpis_stevilka_vpisa=vpis_stevilka_vpisa)[0]
    v.save()
    return v


def add_predmet(sifra, naziv, st_kreditov, idmodul):
    p = Predmet.objects.get_or_create(sifra_predmeta=sifra, idmodul=idmodul)[0]
    p.naziv = naziv
    #p.idmodul = idmodul
    p.st_kreditov = st_kreditov
    p.save()
    return p


def add_profesor(sifra_profesorja, ime, priimek):
    p = Profesor.objects.get_or_create(sifra_profesorja=sifra_profesorja)[0]
    p.ime = ime
    p.priimek = priimek
    p.save()
    return p


def add_izvedba_predmeta(idizvedbe, studijsko_leto, sifra_profesorja, idpredmetnik_stud_programa):
    i = IzvedbaPredmeta.objects.get_or_create(idizvedbe=idizvedbe, studijsko_leto=studijsko_leto,
                                              sifra_profesorja=sifra_profesorja,
                                              idpredmetnik_stud_programa=idpredmetnik_stud_programa)[0]
    i.save()
    return i


def add_studijski_program(idstudijski_program, naziv):
    s = StudijskiProgram.objects.get_or_create(idstudijski_program=idstudijski_program)[0]
    s.naziv = naziv
    s.save()
    return s


def add_vpis(stevilka_vpisa, studijsko_leto, student_vpisna, idstudijski_program, id_letnik, idvrsta_vpisa,
             idnacin_studija, idoblika_studija):
    v = Vpis.objects.get_or_create(stevilka_vpisa=stevilka_vpisa, studijsko_leto=studijsko_leto,
                                   student_vpisna=student_vpisna, idstudijski_program=idstudijski_program,
                                   idletnik=id_letnik, idvrsta_vpisa=idvrsta_vpisa, idnacin_studija=idnacin_studija,
                                   idoblika_studija=idoblika_studija)[0]
    v.save()
    return v


def add_predmetnik_stud_programa(idpredmetnik_stud_programa, letnik, predmet, idsestavni_del_predmetnika,
                                 idstudijski_program):
    p = \
        PredmetnikStudPrograma.objects.get_or_create(idpredmetnik_stud_programa=idpredmetnik_stud_programa,
                                                     letnik=letnik,
                                                     predmet=predmet,
                                                     idstudijski_program=idstudijski_program)[0]
    p.save()
    return p


def add_student(vpisna, ime, priimek, naslov, spol, datum_rojstva, obcina, drzavljanstvo, drzava_rojstva,
                posta_postna_stevilka, davcna, emso, telefonska, naslov_vrocenja, posta_vrocenja, email, geslo):
    s = Student.objects.get_or_create(vpisna=vpisna, datum_rojstva=datum_rojstva, obcina=obcina,
                                      drzavljanstvo=drzavljanstvo, drzava_rojstva=drzava_rojstva,
                                      posta_postna_stevilka=posta_postna_stevilka, davcna=davcna, emso=emso,
                                      posta_vrocenja=posta_vrocenja)[0]
    s.ime = ime
    s.priimek = priimek
    s.naslov = naslov
    s.spol = spol
    # s.datum_rojstva = datum_rojstva
    s.obcina = obcina
    s.drzavljanstvo = drzavljanstvo
    s.drzava_rojstva = drzava_rojstva
    s.posta_postna_stevilka = posta_postna_stevilka
    s.davcna = davcna
    s.emso = emso
    s.telefonska = telefonska
    s.naslov_vrocenja = naslov_vrocenja
    s.posta_vrocenja = posta_vrocenja
    s.email = email
    s.geslo = geslo
    s.save()
    return s


def add_modul(idmodul, name):
    m = Modul.objects.get_or_create(idmodul=idmodul, name=name)[0]
    m.idmodul = idmodul
    m.name = name
    m.save()
    return m


def add_user_student(iduser, email, geslo):
    u = User.objects.get_or_create(iduser=iduser, email=email)[0]
    u.iduser = iduser
    u.email = email
    u.geslo = geslo
    u.role = 'student'
    u.save()
    return u


def add_user_referentka(iduser, email, geslo):
    u = User.objects.get_or_create(iduser=iduser, email=email)[0]
    u.iduser = iduser
    u.email = email
    u.geslo = geslo
    u.role = 'referentka'
    u.save()
    return u


def add_user_profesor(iduser, email, geslo):
    u = User.objects.get_or_create(iduser=iduser, email=email)[0]
    u.iduser = iduser
    u.email = email
    u.geslo = geslo
    u.role = 'profesor'
    u.save()
    return u


def add_sifranti():
    """
    Vzpostavi bazo: prebere in shrani vse potrebne šifrante, ter ustvari administratorja - referentko
    """
    #prebere in shrani vse države iz šifranta
    add_drzave('sifranti/sifrant_drzave.csv')

    #prebere in shrani seznam slovenskih občin
    add_obcine('sifranti/seznam_obcin.csv')

    #prebere in shrani seznam slovenskih pošt(nih števlik)
    add_poste('sifranti/seznam_poste.csv')

    #doda načine študija
    add_nacin_studija()

    #doda vrste vpisa
    add_vrsta_vpisa()

    #doda oblike študija
    add_oblika_studija()

    user_referentka = add_user_referentka(1, 'referentka@email.com', 'geslo')


def populate_db():
    add_sifranti()

    sp = add_studijski_program(1, 'Računalništvo in informatika')

    student = util.create_minimal_student(ime='Primer', priimek='Primerkovič', program='Računalništvo in informatika',
                                          email='primer@gmail.com')

    student2 = util.create_minimal_student(ime='Janez', priimek='Brajdič', program='Računalništvo in informatika',
                                           email='primer2@gmail.com')

    student3 = util.create_minimal_student(ime='Wayne', priimek='Rooney', program='Računalništvo in informatika',
                                           email='primer3@gmail.com')

    student4 = util.create_minimal_student(ime='Gorazd', priimek='Čorba', program='Računalništvo in informatika',
                                           email='primer4@gmail.com')

    student5 = util.create_student_pero(ime='Ahmedin', priimek='Sulejmanivić', program='Računalništvo in informatika',
                                        email='primer5@gmail.com')

    student6 = util.create_student_pero(ime='Luis', priimek='Suarez', program='Računalništvo in informatika',
                                        email='primer6@gmail.com')

    student7 = util.create_student_pero(ime='Toni', priimek='Kroos', program='Računalništvo in informatika',
                                        email='primer7@gmail.com')

    student8 = util.create_student_pero(ime='Raul', priimek='Gonzalez', program='Računalništvo in informatika',
                                        email='primer8@gmail.com')

    o = Obvestilo(idobvestilo=1, student=student, tekst='Pozdravljeni v e-študisu!')
    o.save()

    modul = add_modul(1, "Programiranje")

    modul2 = add_modul(2, "Grafika")

    modul3 = add_modul(3, "Hardware")

    modul4 = add_modul(4, "Prosto")

    date = datetime.date(1992, 1, 1)

    predmet = add_predmet("635640", "TPO", "6", modul)

    predmet1 = add_predmet("635650", "SP", "6", modul)

    predmet2 = add_predmet("635660", "RGTI", "6", modul2)

    predmet3 = add_predmet("635670", "ARS3", "6", modul3)

    predmet4 = add_predmet("635680", "Sportna vzgoja", "3", modul4)

    prof = add_profesor(sifra_profesorja=1, ime='Profesor', priimek='Profi')

    user_profesor = add_user_profesor(99, 'profesor@email.com', 'geslo')

    vpis = Vpis.objects.get(stevilka_vpisa=5)
    vpis2 = Vpis.objects.get(stevilka_vpisa=6)
    vpis3 = Vpis.objects.get(stevilka_vpisa=7)
    vpis4 = Vpis.objects.get(stevilka_vpisa=8)

    ir = IzpitniRok(idizpitni_rok=1, datum=timezone.now(), predmet=predmet)
    ir.save()

    ir_prijava = IzpitniRokPrijava(idizpitni_rok_prijava=1, izpitni_rok=ir, student=student)
    ir_prijava.save()

    vp = add_vpisani_predmeti(1, predmet, vpis)
    vp1 = add_vpisani_predmeti(10, predmet1, vpis)
    vp2 = add_vpisani_predmeti(2, predmet2, vpis2)
    vp3 = add_vpisani_predmeti(3, predmet3, vpis3)
    vp4 = add_vpisani_predmeti(4, predmet4, vpis4)
    vp5 = add_vpisani_predmeti(5, predmet, vpis2)
    vp6 = add_vpisani_predmeti(6, predmet2, vpis)
    vp7 = add_vpisani_predmeti(7, predmet, vpis3)
    vp8 = add_vpisani_predmeti(8, predmet3, vpis)
    vp9 = add_vpisani_predmeti(9, predmet, vpis4)
    vp9 = add_vpisani_predmeti(11, predmet1, vpis4)


if __name__ == '__main__':
    print "Starting population script..."
    populate_db()