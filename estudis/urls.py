__author__ = 'marko'
from django.conf.urls import patterns, include, url

from estudis import views


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^search/$', views.student_search, name='search'),
    url(r'^student_list/$', views.student_list, name='student_list'),
    url(r'^student_list_pdf/$', views.student_list_pdf, name='student_list_pdf'),
    url(r'^student_list_csv/$', views.student_list_csv, name='student_list_csv'),
    url(r'^student_list_expanded/$', views.student_list_expanded, name='student_list_expanded'),
    url(r'^student_list_expanded_pdf/$', views.student_list_expanded_pdf, name='student_list_expanded_pdf'),
    url(r'^student_list_expanded_csv/$', views.student_list_expanded_csv, name='student_list_expanded_csv'),
    url(r'^student_list_choice/$', views.student_list_choice, name='student_list_choice'),
    url(r'^student_data/$', views.student_data, name='student_data'),
    url(r'^personal_information/$', views.student_enrollment_personal_information, name='personal_information'),
    url(r'^enrollment_information/$', views.student_enrollment_information, name='enrollment_information'),
    url(r'^add_students/$', views.add_students, name='add_students'),
    url(r'^referentka_feed/$', views.referentka_feed, name='referentka_feed'),
    url(r'^referentka_feed/(?P<vpisna_st>[0-9]{8})/confirm/$', views.confirm_enrollment, name='confirm_enrollment'),
    url(r'^referentka_feed/(?P<vpisna_st>[0-9]{8})/print/$', views.print_enrollment, name='print_enrollment'),
    url(r'^referentka_feed/(?P<vpisna_st>[0-9]{8})/render_certificat/(?P<st_potrdil>[1-9])/$', views.render_certificat, name='render_certificat'),
    url(r'^optional_subjects/$', views.optional_subjects, name='optional_subjects'),
    url(r'^exam_entry/$', views.exam_entry, name='exam_entry'),
    url(r'^exam_term_find/$', views.exam_term_find, name='exam_term_find'),
    url(r'^exam_term_find/(?P<ir_id>[0-9]+)/exam_term_delete/$', views.exam_term_delete, name='exam_term_delete'),
    url(r'^exam_term_find/(?P<ir_id>[0-9]+)/exam_term_edit/$', views.exam_term_edit, name='exam_term_edit'),
)