# -*- coding: utf-8 -*-

import random
import sys
import string

from django.shortcuts import render, redirect, render_to_response, RequestContext
from django.core.mail import send_mail

from estudis.models import Student, Document, Blocked, User, Vpis, Modul, Predmet, VpisaniPredmeti, VrstaVpisa, \
    Sklep, IzpitniRok, Obvestilo

from estudis.forms import StudentLoginForm, StudentSearchForm, PasswordResetForm, \
    StudentEnrollmentPersonalInformationForm, AddStudentsToDataBase, StudentListForm, StudentListExpandedForm, \
    StudentEnrollmentInformationForm, ExamEntryForm, StudentDataForm, IzpitniRokPrijava, ExamTermEditForm

from estudis.utility import read_student_info, render_to_pdf

from datetime import datetime, timedelta, date
from itertools import izip
from django.http import HttpResponse
from django.template import loader, Context


def index(request):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    role = request.session.get('role')
    context = {}
    if role == 'student':
        email = request.session.get('email')
        s = Student.objects.get(email=email)
        obvestila = Obvestilo.objects.filter(student=s)
        context = {'obvestila': obvestila}

    return render_to_response('estudis/index.html', context, context_instance=RequestContext(request))


def login(request):
    if request.session.get('logged_in'):
        del request.session['logged_in']

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    if 'attemps' not in request.session:
        request.session['attemps'] = 0

    # login form view
    if request.method == 'POST' and 'login' in request.POST:  # if request from login form
        form_login = StudentLoginForm(request.POST, request=request)

        if form_login.is_valid():  #if from data is valid, log user in and redirect to index
            request.session['logged_in'] = True
            email = form_login.cleaned_data['email']
            request.session['email'] = email
            user = User.objects.get(email=email)  #dodal sem da se v sejo vpise role
            request.session['role'] = user.role
            request.session['attemps'] = 0
            if Blocked.objects.filter(ip=ip).exists():
                Blocked.objects.filter(ip=ip).delete()

            return redirect('index')
        else:
            request.session['attemps'] += 1
            if request.session['attemps'] % 3 == 0:
                locktime = datetime.now()
                b = Blocked.objects.get_or_create(ip=ip)[0]
                locktime_str = locktime.strftime("%c")
                b.locktime_str = locktime_str
                b.save()

    else:
        form_login = StudentLoginForm(request.POST, request=request)  # generate blank login form

    # password reset view
    if request.method == 'POST' and 'password-reset' in request.POST:  #if request from reset form
        form_reset = PasswordResetForm(request.POST)

        if form_reset.is_valid():  #if form data is valid, reset and email new password
            s = Student.objects.get(email=request.POST['email'])
            email_text = "Vaše novo geslo je: "
            new_password = (
                ''.join(random.choice(string.ascii_uppercase) for i in range(8)))  #generate random 8 char string
            email_text += new_password
            s.geslo = new_password  #TODO: encryption
            s.save()
            send_mail('ESTUDIS - Ponastavitev gesla', email_text, 'from@example.com',
                      [s.email])  #mail student new password

            return redirect('index')
    else:
        form_reset = PasswordResetForm()  #generate blank reset form

    context = {'form_login': form_login, 'form_reset': form_reset}
    return render_to_response('estudis/login.html', context, context_instance=RequestContext(request))


def student_search(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')
    students = Student.objects.all()
    if request.method == 'POST':
        form = StudentSearchForm(request.POST)

        if form.is_valid():
            if 'name' in form.cleaned_data:
                name = form.cleaned_data['name']
                students = students.filter(ime__istartswith=name)
                display_list = True
            if 'surname' in form.cleaned_data:
                surname = form.cleaned_data['surname']
                students = students.filter(priimek__istartswith=surname)
                display_list = True
            if 'enrollment_no' in form.cleaned_data:
                try:
                    enrollment_no = int(form.cleaned_data['enrollment_no'])
                    if Student.objects.filter(vpisna=enrollment_no).exists():
                        students = Student.objects.filter(vpisna=enrollment_no)
                        display_list = True
                except ValueError:
                    pass
    else:
        form = StudentSearchForm()
    context = {'form': form, 'students': students, 'display_list': display_list}
    return render_to_response('estudis/search.html', context, context_instance=RequestContext(request))


def student_enrollment_personal_information(request):
    logged_in = request.session.get('logged_in')
    email = request.session.get('email')

    if not logged_in:
        return redirect('login')

    if request.session.get('role') != 'student':
        return redirect('index')
    s = Student.objects.get(email=email)
    vpisi_student = Vpis.objects.filter(student_vpisna=s)
    vpisi_nekoncani = vpisi_student.filter(koncan_vpis=0)
    vpisi_koncani = vpisi_student.filter(koncan_vpis=1)
    for vp in vpisi_koncani:
        cur_leto = str(date.today().year) + '/' + str(date.today().year + 1)
        if vp.studijsko_leto == cur_leto:
            return redirect('index')

    if vpisi_nekoncani.count() == 0:
        star_vpis = vpisi_koncani[vpisi_koncani.count()-1]
        v = Vpis(stevilka_vpisa=Vpis.objects.count() + 1, student_vpisna=s,
                 idstudijski_program=star_vpis.idstudijski_program, letnik=star_vpis.letnik)
        v.save()

    if request.method == 'POST':
        form = StudentEnrollmentPersonalInformationForm(email, request.POST)
        if form.is_valid():
            request.session['st_vpisa'] = form.cleaned_data['st_vpisa']
            return redirect('enrollment_information')
    else:
        form = StudentEnrollmentPersonalInformationForm(email_in=email)

    context = {'form': form}
    return render_to_response('estudis/personal_information.html', context, context_instance=RequestContext(request))


def student_enrollment_information(request):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    email = request.session.get('email')
    st_vpisa = request.session.get('st_vpisa')
    if request.method == 'POST':
        form = StudentEnrollmentInformationForm(st_vpisa, request.POST)
        if form.is_valid():
            return redirect('index')
    else:
        form = StudentEnrollmentInformationForm(st_vpisa_in=st_vpisa)

    context = {'form': form}
    return render_to_response('estudis/enrollment_information.html', context, context_instance=RequestContext(request))


def add_students(request):  # logged_in = request.session.get('logged_in') # preveri sejo
    feedback = [0, 0]
    # nalaganje dokumenta - shranjevanje v bazo
    if request.method == 'POST':
        form = AddStudentsToDataBase(request.POST, request.FILES)
        if form.is_valid():
            dobljeno_ime = request.FILES['ime_dokumenta']
            newdoc = Document(ime_dokumenta=dobljeno_ime)
            newdoc.save()

            try:
                feedback = read_student_info(newdoc.ime_dokumenta.url)
            except ValueError:
                pass

            # po nalaganju preusmerimo nazaj
            # return HttpResponseRedirect(reverse('estudis.views.add_students'), args=(feedback[1],))
            form = AddStudentsToDataBase()
            return render_to_response('estudis/add_students.html', {'feedback': feedback, 'form': form},
                                      context_instance=RequestContext(request))
    else:
        form = AddStudentsToDataBase()  # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'estudis/add_students.html',
        {'documents': documents, 'feedback': feedback, 'form': form},
        context_instance=RequestContext(request)
    )


def optional_subjects(request):  # funkcija za izbirne predmete
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    moduli = Modul.objects.all().exclude(name='Prosto')
    prvimodul = moduli[0]
    moduli = moduli[1:]
    predmeti = Predmet.objects.all()
    id_prosto = Modul.objects.all().filter(name='Prosto').values('idmodul')
    prosto_izbirni = Predmet.objects.all().filter(idmodul=id_prosto)
    return render_to_response('estudis/optional_subjects.html',
                              {'moduli': moduli, 'predmeti': predmeti, 'prvimodul': prvimodul,
                               'prosto_izbirni': prosto_izbirni})


def referentka_feed(request):  # pokaze nepotrjene studente, ki jih lahko potem referentka potrdi, ali izda potrdilo

    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')

    vsi_potrjeni = True
    students = Student.objects.all()

    nepotrjeni = Vpis.objects.filter(potrjen_vpis=0)
    if nepotrjeni.count() > 0:
        students = Student.objects.filter(vpisna__in=nepotrjeni.values("student_vpisna"))  # nepotrjeni studenti
        vsi_potrjeni = False

    enrolled_students = Student.objects.exclude(vpisna__in=nepotrjeni.values("student_vpisna"))  # vpisani studenti

    form = StudentSearchForm()
    context = {'form': form, 'students': students, 'enrolled_students': enrolled_students, 'vsi_potrjeni': vsi_potrjeni}
    return render_to_response('estudis/referentka_feed.html', context, context_instance=RequestContext(request))


def confirm_enrollment(request, vpisna_st):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')

    student = Student.objects.filter(vpisna=vpisna_st)
    vpis_studenta = Vpis.objects.get(student_vpisna=vpisna_st)

    vpis_studenta.potrjen_vpis = 1
    vpis_studenta.save()

    context = {'student': student, 'vpis_studenta': vpis_studenta}
    return render_to_response('estudis/enrollment_certificate.html', context, context_instance=RequestContext(request))


def print_enrollment(request, vpisna_st):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')

    student = Student.objects.filter(vpisna=vpisna_st)
    vpis_studenta = Vpis.objects.get(student_vpisna=vpisna_st)

    context = {'student': student, 'vpis_studenta': vpis_studenta}
    return render_to_response('estudis/print_enrollment.html', context, context_instance=RequestContext(request))


def render_certificat(request, vpisna_st, st_potrdil):
    student = Student.objects.filter(vpisna=vpisna_st)
    vpis_studenta = Vpis.objects.get(student_vpisna=vpisna_st)
    st_potrdil = int(st_potrdil)

    st_izdanih = vpis_studenta.stevec_potrdil
    # st_izdanih += 1

    potrdila = range(st_izdanih, st_izdanih+st_potrdil)

    vpis_studenta.stevec_potrdil = st_izdanih+st_potrdil
    vpis_studenta.save()

    return render_to_pdf(
        'estudis/template_enrollment_certificate.html',
        {
            'pagesize': 'A4',
            'student': student,
            'vpis_studenta': vpis_studenta,
            'potrdila': potrdila,
        }
    )


def student_list(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')
    students = Student.objects.all()
    vpis_filter = Vpis.objects.all()
    list3 = []

    if request.method == 'POST':
        form = StudentListForm(request.POST)

        if form.is_valid():
            if 'subject' in form.cleaned_data:
                sifra = form.cleaned_data['subject']
                leto = form.cleaned_data['year']

                request.session['sifra_predmeta'] = int(sifra)
                request.session['leto'] = leto

                vpisani_predmeti_filter = VpisaniPredmeti.objects.filter(sifra_predmeta_id=int(sifra))

                list1 = []
                for x in vpisani_predmeti_filter:
                    list1.append(str(x.vpis_stevilka_vpisa.stevilka_vpisa))
                vpf = set(list1)
                vpis_filter = Vpis.objects.filter(stevilka_vpisa__in=vpf, studijsko_leto=leto)

                list2 = []
                for x in vpis_filter:
                    list2.append(str(x.student_vpisna.vpisna))
                vfs1 = set(list2)

                students = Student.objects.filter(vpisna__in=vfs1)

                students = students.order_by('priimek')
                vpis_filter = vpis_filter.order_by('student_vpisna__priimek')

                display_list = True

                list3 = range(1, students.__len__() + 1)
    else:
        form = StudentListForm()

    context = {'form': form, 'display_list': display_list, 'iter_data': izip(students, vpis_filter, list3)}
    return render_to_response('estudis/student_list.html', context, context_instance=RequestContext(request))


def student_list_pdf(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')

    students = Student.objects.all()
    vpis_filter = Vpis.objects.all()
    list3 = []

    sifra = request.session['sifra_predmeta']
    leto = request.session['leto']

    vpisani_predmeti_filter = VpisaniPredmeti.objects.filter(sifra_predmeta_id=int(sifra))

    list1 = []
    for x in vpisani_predmeti_filter:
        list1.append(str(x.vpis_stevilka_vpisa.stevilka_vpisa))
    vpf = set(list1)
    vpis_filter = Vpis.objects.filter(stevilka_vpisa__in=vpf, studijsko_leto=leto)

    list2 = []
    for x in vpis_filter:
        list2.append(str(x.student_vpisna.vpisna))
    vfs1 = set(list2)

    students = Student.objects.filter(vpisna__in=vfs1)

    students = students.order_by('priimek')
    vpis_filter = vpis_filter.order_by('student_vpisna__priimek')

    display_list = True

    list3 = range(1, students.__len__() + 1)

    return render_to_pdf('estudis/student_list_pdf.html',
                         {'pagesize': 'A4', 'iter_data': izip(students, vpis_filter, list3),
                          'display_list': display_list})


def student_list_csv(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="student_list.csv"'

    students = Student.objects.all()
    vpis_filter = Vpis.objects.all()
    list3 = []

    sifra = request.session['sifra_predmeta']
    leto = request.session['leto']

    vpisani_predmeti_filter = VpisaniPredmeti.objects.filter(sifra_predmeta_id=int(sifra))

    list1 = []
    for x in vpisani_predmeti_filter:
        list1.append(str(x.vpis_stevilka_vpisa.stevilka_vpisa))
    vpf = set(list1)
    vpis_filter = Vpis.objects.filter(stevilka_vpisa__in=vpf, studijsko_leto=leto)

    list2 = []
    for x in vpis_filter:
        list2.append(str(x.student_vpisna.vpisna))
    vfs1 = set(list2)

    students = Student.objects.filter(vpisna__in=vfs1)

    students = students.order_by('priimek')
    vpis_filter = vpis_filter.order_by('student_vpisna__priimek')

    list3 = range(1, students.__len__() + 1)

    iter_data = izip(students, vpis_filter, list3)

    t = loader.get_template('estudis/student_list_csv.txt')
    c = Context({
        'iter_data': iter_data,
    })

    response.write(t.render(c))
    return response


def student_list_choice(request):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')

    return render(request, 'estudis/student_list_choice.html')


def student_list_expanded(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')

    students = Student.objects.all()
    vpis = Vpis.objects.all()
    predmeti = Predmet.objects.all()
    vpisani_predmeti = VpisaniPredmeti.objects.all()
    list1 = []

    if request.method == 'POST':
        form = StudentListExpandedForm(request.POST)

        if form.is_valid():
            leto = form.cleaned_data['leto_dropdown']
            letnik = form.cleaned_data['letnik_dropdown']
            nacin = form.cleaned_data['nacin_dropdown']
            vrsta = form.cleaned_data['vrsta_dropdown']
            program = form.cleaned_data['program_dropdown']
            modul = form.cleaned_data['modul_dropdown']

            request.session['leto'] = leto
            request.session['letnik'] = letnik
            request.session['nacin'] = nacin
            request.session['vrsta'] = vrsta
            request.session['program'] = program
            request.session['modul'] = modul

            if letnik != '-':
                vpis = vpis.filter(letnik=letnik)
            if leto != '-':
                vpis = vpis.filter(studijsko_leto=leto)
            if nacin != '-':
                vpis = vpis.filter(idnacin_studija=nacin)
            if vrsta != '-':
                vpis = vpis.filter(idvrsta_vpisa=vrsta)
            if program != '-':
                vpis = vpis.filter(idstudijski_program=program)

            if modul != '-':
                predmeti = predmeti.filter(idmodul=int(modul))
                sifra = []
                for x in predmeti:
                    sifra.append(x.sifra_predmeta)

                vpisani_predmeti = VpisaniPredmeti.objects.filter(sifra_predmeta_id__in=sifra)
                stevilke = []
                for x in vpisani_predmeti:
                    stevilke.append(x.vpis_stevilka_vpisa.stevilka_vpisa)
                stevilke_set = set(stevilke)
                vpis = vpis.filter(stevilka_vpisa__in=stevilke_set)

            vpisne = []
            for x in vpis:
                vpisne.append(x.student_vpisna.vpisna)
            students = students.filter(vpisna__in=vpisne)

            students.order_by('priimek')
            vpis.order_by('student_vpisna__priimek')

            display_list = True

            list1 = range(1, students.__len__() + 1)

    else:
        form = StudentListExpandedForm()

    context = {'form': form, 'display_list': display_list, 'iter_data': izip(list1, students, vpis)}
    return render_to_response('estudis/student_list_expanded.html', context, context_instance=RequestContext(request))


def student_list_expanded_pdf(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')

    students = Student.objects.all()
    vpis = Vpis.objects.all()
    predmeti = Predmet.objects.all()
    vpisani_predmeti = VpisaniPredmeti.objects.all()
    list1 = []

    leto = request.session['leto']
    letnik = request.session['letnik']
    nacin = request.session['nacin']
    vrsta = request.session['vrsta']
    program = request.session['program']
    modul = request.session['modul']

    if letnik != '-':
        vpis = vpis.filter(letnik=letnik)
    if leto != '-':
        vpis = vpis.filter(studijsko_leto=leto)
    if nacin != '-':
        vpis = vpis.filter(idnacin_studija=nacin)
    if vrsta != '-':
        vpis = vpis.filter(idvrsta_vpisa=vrsta)
    if program != '-':
        vpis = vpis.filter(idstudijski_program=program)

    if modul != '-':
        predmeti = predmeti.filter(idmodul=int(modul))
        sifra = []
        for x in predmeti:
            sifra.append(x.sifra_predmeta)

        vpisani_predmeti = VpisaniPredmeti.objects.filter(sifra_predmeta_id__in=sifra)
        stevilke = []
        for x in vpisani_predmeti:
            stevilke.append(x.vpis_stevilka_vpisa.stevilka_vpisa)
        stevilke_set = set(stevilke)
        vpis = vpis.filter(stevilka_vpisa__in=stevilke_set)

    vpisne = []
    for x in vpis:
        vpisne.append(x.student_vpisna.vpisna)
    students = students.filter(vpisna__in=vpisne)

    students.order_by('priimek')
    vpis.order_by('student_vpisna__priimek')

    display_list = True

    list1 = range(1, students.__len__() + 1)

    return render_to_pdf('estudis/student_list_pdf.html',
                         {'pagesize': 'A4', 'iter_data': izip(students, vpis, list1),
                          'display_list': display_list})


def student_list_expanded_csv(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    if not logged_in:
        return redirect('login')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="student_list.csv"'

    students = Student.objects.all()
    vpis = Vpis.objects.all()
    predmeti = Predmet.objects.all()
    vpisani_predmeti = VpisaniPredmeti.objects.all()
    list1 = []

    leto = request.session['leto']
    letnik = request.session['letnik']
    nacin = request.session['nacin']
    vrsta = request.session['vrsta']
    program = request.session['program']
    modul = request.session['modul']

    if letnik != '-':
        vpis = vpis.filter(letnik=letnik)
    if leto != '-':
        vpis = vpis.filter(studijsko_leto=leto)
    if nacin != '-':
        vpis = vpis.filter(idnacin_studija=nacin)
    if vrsta != '-':
        vpis = vpis.filter(idvrsta_vpisa=vrsta)
    if program != '-':
        vpis = vpis.filter(idstudijski_program=program)

    if modul != '-':
        predmeti = predmeti.filter(idmodul=int(modul))
        sifra = []
        for x in predmeti:
            sifra.append(x.sifra_predmeta)

        vpisani_predmeti = VpisaniPredmeti.objects.filter(sifra_predmeta_id__in=sifra)
        stevilke = []
        for x in vpisani_predmeti:
            stevilke.append(x.vpis_stevilka_vpisa.stevilka_vpisa)
        stevilke_set = set(stevilke)
        vpis = vpis.filter(stevilka_vpisa__in=stevilke_set)

    vpisne = []
    for x in vpis:
        vpisne.append(x.student_vpisna.vpisna)
    students = students.filter(vpisna__in=vpisne)

    students.order_by('priimek')
    vpis.order_by('student_vpisna__priimek')

    display_list = True

    list1 = range(1, students.__len__() + 1)

    iter_data = izip(students, vpis, list1)

    t = loader.get_template('estudis/student_list_csv.txt')
    c = Context({
        'iter_data': iter_data,
    })

    response.write(t.render(c))
    return response


def exam_entry(request):
    feedback = ['', '']
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    role = request.session.get('role')
    if role != 'referentka':
        return redirect('index')
    if request.method == 'POST':
        form = ExamEntryForm(request.POST)
        if form.is_valid():
            feedback[0] = form.cleaned_data['subject']
            feedback[1] = form.cleaned_data['exam_date']
            #context = {'form': form, 'feedback': feedback}
            #return render_to_response('estudis/exam_entry.html', context, context_instance=RequestContext(request))
    else:
        form = ExamEntryForm()
    context = {'form': form, 'feedback': feedback}
    return render_to_response('estudis/exam_entry.html', context, context_instance=RequestContext(request))


def student_data(request):
    logged_in = request.session.get('logged_in')
    display_list = False
    display_sklepi = False
    if not logged_in:
        return redirect('login')

    students = Student.objects.all()
    sklepi = Sklep.objects.all()

    if request.method == 'POST':
        form = StudentDataForm(request.POST)

        if form.is_valid():
            if 'enrollment_no' in form.cleaned_data:
                try:
                    enrollment_no = int(form.cleaned_data['enrollment_no'])
                    if Student.objects.filter(vpisna=enrollment_no).exists():
                        students = Student.objects.filter(vpisna=enrollment_no)
                        display_list = True

                    if Sklep.objects.filter(vpisna=enrollment_no).exists():
                        sklepi = sklepi.filter(vpisna=enrollment_no)
                        display_sklepi = True
                except ValueError:
                    pass
    else:
        form = StudentDataForm()
    context = {'form': form, 'students': students, 'display_list': display_list, 'display_sklepi': display_sklepi, 'sklepi': sklepi}
    return render_to_response('estudis/student_data.html', context, context_instance=RequestContext(request))


def exam_term_find(request):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    role = request.session.get('role')
    if role != 'profesor' and role != 'referentka':
        return redirect('index')

    prijavljeni = IzpitniRokPrijava.objects.values('izpitni_rok').distinct()
    ir_prijavljeni = IzpitniRok.objects.filter(idizpitni_rok=prijavljeni)
    if prijavljeni.count() > 0:
        ir_brez_prijav = IzpitniRok.objects.exclude(idizpitni_rok=prijavljeni)
    else:
        ir_brez_prijav = IzpitniRok.objects.all()

    context = {'ir_prijavljeni': ir_prijavljeni, 'ir_brez_prijav': ir_brez_prijav}
    return render_to_response('estudis/exam_term_find.html', context, context_instance=RequestContext(request))


def exam_term_delete(request, ir_id):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    role = request.session.get('role')
    if role != 'profesor' and role != 'referentka':
        return redirect('index')

    ir = IzpitniRok.objects.get(idizpitni_rok=ir_id)
    feedback = ''
    prijavljeni = IzpitniRokPrijava.objects.filter(izpitni_rok=ir).count()
    if prijavljeni > 0:
        feedback = 'Za ta izpitni rok že obstajajo prijave'

    if request.method == 'POST':
        prijave = IzpitniRokPrijava.objects.filter(izpitni_rok=ir)
        for p in prijave:
            p.delete()
        ir.delete()
        return redirect('index')

    context = {'ir': ir, 'feedback': feedback}
    return render_to_response('estudis/exam_term_delete.html', context, context_instance=RequestContext(request))


def exam_term_edit(request, ir_id):
    logged_in = request.session.get('logged_in')
    if not logged_in:
        return redirect('login')
    role = request.session.get('role')
    if role != 'profesor' and role != 'referentka':
        return redirect('index')

    ir = IzpitniRok.objects.get(idizpitni_rok=ir_id)
    feedback = ''
    prijavljeni = IzpitniRokPrijava.objects.filter(izpitni_rok=ir)
    if prijavljeni.count() > 0:
        feedback = 'Za ta izpitni rok že obstajajo prijave'

    if request.method == 'POST':
        form = ExamTermEditForm(ir, request.POST)
        if form.is_valid():
            new_date = form.cleaned_data['new_date']
            ir.datum = new_date
            ir.save()
            for p in prijavljeni:
                s = p.student
                o = Obvestilo(idobvestilo=Obvestilo.objects.all().count()+1, student=s, tekst='Sprememba izpitnega roka: ' + ir.__unicode__())
                o.save()
        return redirect('index')
    else:
        form = ExamTermEditForm(izpitni_rok_in=ir)

    context = {'ir': ir, 'feedback': feedback, 'form': form}
    return render_to_response('estudis/exam_term_edit.html', context, context_instance=RequestContext(request))
