# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
# from dateutil.relativedelta import relativedelta

from django import forms
from django.utils import timezone
from estudis.models import Student, Drzava, Posta, Vpis, StudijskiProgram, VrstaVpisa, Blocked, User, Predmet, \
    NacinStudija, Modul, Obcina, IzpitniRok, OblikaStudija, IzpitniRokPrijava
from estudis import utility


class StudentLoginForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(StudentLoginForm, self).__init__(*args, **kwargs)

    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Email:', max_length=200,
                             error_messages={'required': 'Manjkajoč email', 'max_length': 'Predolg email',
                                             'invalid': 'Napačna oblika'})
    password = forms.CharField(label='Geslo:', widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               max_length=40,
                               error_messages={'required': 'Manjkajoče geslo', 'max_length': 'Predolgo geslo'})

    def clean(self):
        form_data = self.cleaned_data

        if 'email' not in form_data:
            return form_data

        if 'password' not in form_data:
            return form_data

        email = form_data['email']
        password = form_data['password']

        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')

        if Blocked.objects.filter(ip=ip).exists():
            cur_time = datetime.now()
            b = Blocked.objects.get(ip=ip)
            locktime = datetime.strptime(b.locktime_str, "%c")
            dif = cur_time - locktime

            if dif.seconds < 300:
                raise forms.ValidationError("Preveč neveljavnih poskusov! Poskusite ponovno čez 5 minut.")

        try:
            u = User.objects.get(email=email)
            if u.geslo != password:  # TODO: encryption
                raise forms.ValidationError("Napačno geslo")
        except User.DoesNotExist:
            raise forms.ValidationError("Uporabnik s tem email-om ne obstaja")

        return form_data


class PasswordResetForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Email:', max_length=200,
                             error_messages={'required': 'Manjkajoč email', 'max_length': 'Predolg email',
                                             'invalid': 'Napačna oblika'})

    def clean(self):
        form_data = self.cleaned_data
        if 'email' not in form_data:
            return form_data

        email = form_data['email']
        try:
            u = User.objects.get(email=email)
        except Student.DoesNotExist:
            raise forms.ValidationError("Uporabnik s tem email-om ne obstaja")

        return form_data


class StudentSearchForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Ime:', required=False)
    surname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Priimek:', required=False)
    enrollment_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Vpisna številka:',
                                    required=False)


class StudentListForm(forms.Form):
    subjects = Predmet.objects.all()
    vpisi = Vpis.objects.all().values_list('studijsko_leto', flat=True).distinct()

    leto = []
    for x in vpisi:
        leto.append(x)
    tuples1 = zip(leto, leto)

    ids = []
    names = []
    for x in subjects:
        ids.append(x.sifra_predmeta)
        names.append(x.naziv)
    tuples2 = zip(ids, names)

    subject = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples2, label="Predmet")
    year = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples1,
                             label="Študijsko leto")


class StudentListExpandedForm(forms.Form):
    vpis_leto = Vpis.objects.all().values_list('studijsko_leto', flat=True).distinct()
    vpis_letnik = Vpis.objects.all().values_list('letnik', flat=True).distinct()
    vpis_program = Vpis.objects.all().values_list('idstudijski_program', flat=True).distinct()
    studijski_program = StudijskiProgram.objects.filter(idstudijski_program__in=vpis_program)
    vpis_vrsta = Vpis.objects.all().values_list('idvrsta_vpisa', flat=True).distinct()
    vrsta_vpisa = VrstaVpisa.objects.filter(idvrsta_vpisa__in=vpis_vrsta)
    vpis_nacin = Vpis.objects.all().values_list('idnacin_studija', flat=True).distinct()
    nacin_studija = NacinStudija.objects.filter(idnacin_studija__in=vpis_nacin)
    moduli = Modul.objects.all()

    leto = ['-']
    for x in vpis_leto:
        leto.append(x)
    tuples1 = zip(leto, leto)

    letnik = ['-']
    for x in vpis_letnik:
        letnik.append(x)
    tuples2 = zip(letnik, letnik)

    program = ['-']
    idprogram = ['-']
    for x in vpis_program:
        idprogram.append(x)
    for x in studijski_program:
        program.append(x)
    tuples3 = zip(idprogram, program)

    vrsta = ['-']
    idvrsta = ['-']
    for x in vpis_vrsta:
        if x is not None:
            idvrsta.append(x)
    for x in vrsta_vpisa:
        if x is not None:
            vrsta.append(x)
    tuples4 = zip(idvrsta, vrsta)

    nacin = ['-']
    idnacin = ['-']
    for x in vpis_nacin:
        if x is not None:
            idnacin.append(x)
    for x in nacin_studija:
        if x is not None:
            nacin.append(x)
    tuples5 = zip(idnacin, nacin)

    modul = ['-']
    idmodul = ['-']
    for x in moduli:
        idmodul.append(x.idmodul)
        modul.append(x.name)
    tuples6 = zip(idmodul, modul)

    leto_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples1,
                                      label="Študijsko leto")
    letnik_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples2,
                                        label="Letnik")
    program_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples3,
                                         label="Študijski program")
    vrsta_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples4,
                                       label="Vrsta vpisa")
    nacin_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples5,
                                       label="Nacin studija")
    modul_dropdown = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=tuples6,
                                       label="Modul")


class StudentDataForm(forms.Form):
    enrollment_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Vpisna številka:',
                                    required=False)


class StudentEnrollmentPersonalInformationForm(forms.Form):
    temporary_checkbox = forms.BooleanField(label='Vročanje na naslov začasnega prebivališča', required=False)


    # init overridden to acces enrollment number from session and populate fields
    def __init__(self, email_in, *args, **kwargs):
        super(StudentEnrollmentPersonalInformationForm, self).__init__(*args, **kwargs)
        """
        TODO:
        Ob kliku na gumb 'vpis' naj se bazo doda nov (prazen vpis). Na ta vpis se veže forma. Vpis mora imeti izpolnjena
        polja stevilka_vpisa, student, studijski_program.
        """
        email = email_in

        student = Student.objects.get(email=email_in)
        vpisna = student.vpisna

        #read-only fields (populated from db on load)
        self.fields['enrollment_no'] = forms.CharField(label='Vpisna številka:',
                                                       widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                       initial=vpisna)
        self.fields['name'] = forms.CharField(label='Ime:',
                                              widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                              initial=student.ime)
        self.fields['surname'] = forms.CharField(label='Priimek:',
                                                 widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                 initial=student.priimek)
        self.fields['email'] = forms.CharField(label='Email:',
                                               widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                               initial=student.email)
        vpis = Vpis.objects.filter(student_vpisna=student).order_by('stevilka_vpisa')[0]
        self.fields['st_vpisa'] = forms.CharField(label='Št. vpisa:',
                                                  widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                  initial=vpis.stevilka_vpisa)

        #editable fields    TODO: localize error messages, add custom validation
        #student personal information
        self.fields['date_of_birth'] = forms.DateField(label='Datum rojstva:', required=True, widget=forms.DateInput,
                                                       initial=date.today())
        gender_choices = (
            ('Moški', 'Moški'),
            ('Ženski', 'Ženski'),
        )
        self.fields['gender'] = forms.ChoiceField(widget=forms.RadioSelect, choices=gender_choices, label='Spol:',
                                                  required=True)
        self.fields['ss_no'] = forms.CharField(label='EMŠO:', required=True,
                                               error_messages={'required': 'Manjkajoč EMŠO'})
        self.fields['tax'] = forms.CharField(label='Davčna številka:', required=True,
                                             error_messages={'required': 'Manjkajoča davčna številka'})
        self.fields['cell'] = forms.CharField(label='Prenosni telefon:', required=True,
                                              error_messages={'required': 'Manjkajoča številka prenosnega telefona'})
        self.fields['place_of_birth'] = forms.CharField(label='Kraj rojstva:', required=True)
        country_queryset = Drzava.objects.all().order_by('naziv')
        self.fields['country_of_birth'] = forms.ModelChoiceField(label='Država rojstva:', queryset=country_queryset,
                                                                 required=True)
        self.fields['municipality_of_birth'] = forms.CharField(label='Občina rojstva:', required=True)
        self.fields['citizenship'] = forms.ModelChoiceField(label='Državljanstvo:', queryset=country_queryset,
                                                            required=True)
        post_queryset = Posta.objects.all().order_by('naziv')
        self.fields['permanent_address'] = forms.CharField(label='Naslov stalnega prebivališča', required=True)
        self.fields['permanent_post'] = forms.ModelChoiceField(label='Pošta stalnega prebivališča',
                                                               queryset=post_queryset, required=True)
        self.fields['temporary_address'] = forms.CharField(label='Naslov začasnega prebivališča', required=False)
        self.fields['temporary_post'] = forms.ModelChoiceField(label='Pošta začasnega prebivališča',
                                                               queryset=post_queryset, required=False)


        #check if student has previous enrolment
        vc = Vpis.objects.filter(student_vpisna=student).count()
        if vc > 0:
            vpis = Vpis.objects.filter(student_vpisna=student)[vc - 1]
            self.fields['cell'].initial = student.telefonska
            self.fields['citizenship'].initial = student.drzavljanstvo
            self.fields['country_of_birth'].initial = student.drzava_rojstva
            self.fields['permanent_address'].initial = student.naslov
            self.fields['permanent_post'].initial = student.posta_postna_stevilka


    #validation
    def clean(self):
        form_data = self.cleaned_data
        student = Student.objects.get(vpisna=form_data['enrollment_no'])
        date_of_birth = form_data['date_of_birth']
        eighteen_years_ago = date.today() - timedelta(days=(18 * 365.24))
        if eighteen_years_ago < form_data['date_of_birth']:
            raise forms.ValidationError('Neveljaven datum rojstva')
        #EMSO validation
        ss_no = form_data['ss_no']
        dd = ss_no[0:2]
        mm = ss_no[2:4]
        yyy = ss_no[4:7]
        gen = int(ss_no[7:10])
        day_of_birth = str(date_of_birth.day)
        if len(day_of_birth) == 1:
            day_of_birth = '0' + day_of_birth
        month_of_birth = str(date_of_birth.month)
        if len(month_of_birth) == 1:
            month_of_birth = '0' + month_of_birth

        if not ss_no.isdigit():
            raise forms.ValidationError('EMŠO vsebuje nedovoljene znake')
        if (day_of_birth != dd) or (month_of_birth != mm):
            raise forms.ValidationError('Napačen EMŠO')
        if str(date_of_birth.year)[1:4] != yyy:
            raise forms.ValidationError('Napačen EMŠO')

        gender = form_data['gender']

        if gender == 'Moški':
            if not gen >= 0 and gen <= 499:
                raise forms.ValidationError('Napačen EMŠO')
        else:
            if not gen > 499:
                raise forms.ValidationError('Napačen EMŠO')

        if not utility.check_emso(ss_no):
            raise forms.ValidationError('Napačen EMŠO')

        place_of_birth = form_data['place_of_birth']
        country_of_birth = form_data['country_of_birth']
        municipality_of_birth = form_data['municipality_of_birth']
        citizenship = form_data['citizenship']
        if country_of_birth == 'Slovenija':
            if not Drzava.objects.filter(naziv__iexact=municipality_of_birth):
                raise forms.ValidationError('Država in občina rojstva se ne ujemata')

        student.telefonska = form_data['cell']
        student.davcna = form_data['tax']
        student.spol = form_data['gender']
        student.datum_rojstva = date_of_birth
        student.emso = ss_no
        student.naslov = form_data['permanent_address']
        student.posta_postna_stevilka = form_data['permanent_post']
        if country_of_birth == 'Slovenija':
            student.obcina = Obcina.objects.filter(naziv__iexact=form_data['municipality_of_birth'])[0]
        else:
            Obcina(idobcina=Obcina.objects.all().count() + 1, naziv=form_data['municipality_of_birth'])
        if form_data['temporary_checkbox']:
            student.naslov_vrocanja = form_data['temporary_address']
            student.posta_vrocanja = form_data['temporary_post']
        else:
            student.naslov_vrocanja = form_data['permanent_address']
            student.posta_vrocanja = form_data['permanent_post']
        student.drzavljanstvo = citizenship
        student.drzava_rojstva = country_of_birth

        student.save()

        return form_data


class StudentEnrollmentInformationForm(forms.Form):
    # student enrollment information
    #student lahko izbere vse vrste vpisa razen 98
    vrsta_vpisa_queryset = VrstaVpisa.objects.exclude(idvrsta_vpisa=98)
    nacin_studija_queryset = NacinStudija.objects.all()
    oblika_studija_queryset = OblikaStudija.objects.all()

    vrsta_vpisa = forms.ModelChoiceField(label='Vrsta vpisa:', queryset=vrsta_vpisa_queryset, required=True)
    nacin_studija = forms.ModelChoiceField(label='Način študija:', queryset=nacin_studija_queryset, required=True)
    oblika_studija = forms.ModelChoiceField(label='Oblika študija:', queryset=oblika_studija_queryset, required=True)

    #form mostly populated on load
    def __init__(self, st_vpisa_in, *args, **kwargs):
        super(StudentEnrollmentInformationForm, self).__init__(*args, **kwargs)
        stevilka_vpisa = st_vpisa_in
        vpis = Vpis.objects.get(stevilka_vpisa=stevilka_vpisa)
        student = vpis.student_vpisna

        studijski_program_queryset = StudijskiProgram.objects.all()

        #Initialiy populated fields
        self.fields['vpisna'] = forms.CharField(label='Vpisna številka',
                                                widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                initial=student.vpisna)
        self.fields['st_vpisa'] = forms.CharField(label='Št. vpisa:',
                                                  widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                  initial=vpis.stevilka_vpisa)
        studijsko_leto = str(date.today().year) + '/' + str(date.today().year + 1)
        self.fields['stud_leto'] = forms.CharField(label='Študijsko leto:',
                                                   widget=forms.TextInput(attrs={'readonly': 'readonly'}),
                                                   initial=studijsko_leto)

        self.fields['letnik'] = forms.IntegerField(label='Letnik:', initial=vpis.letnik)
        izbran_program = vpis.idvrsta_vpisa
        self.fields['stud_program'] = forms.ModelChoiceField(label='Študijski program',
                                                             queryset=studijski_program_queryset,
                                                             initial=izbran_program)

    #validation
    def clean(self):
        form_data = self.cleaned_data
        vpisna = form_data['vpisna']
        student = Student.objects.get(vpisna=vpisna)

        vpisi = Vpis.objects.filter(student_vpisna=student)
        vpis_count = vpisi.count()
        if vpis_count > 0:
            prev_vpis = vpisi[vpis_count - 1]
            if form_data['letnik'] < prev_vpis.letnik:
                raise forms.ValidationError('Neveljaven letnik')
        else:
            if form_data['vrsta_vpisa'] != 'Prvi vpis/dodatni letnik':
                raise forms.ValidationError('Za prvi vpis izberite prvi vpis v letnik')
        vp = Vpis.objects.get(stevilka_vpisa=form_data['st_vpisa'])

        vp.idvrsta_vpisa = form_data['vrsta_vpisa']
        vp.idnacin_studija = form_data['nacin_studija']
        vp.idoblika_studija = form_data['oblika_studija']
        vp.letnik = form_data['letnik']
        vp.save()
        if vp.letnik < 3:
            vp.koncan_vpis = 1
        return form_data


class AddStudentsToDataBase(forms.Form):
    ime_dokumenta = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}), label='Izberi datoteko',
                                    help_text='maksimalno 42 MB')


class ReferentkaFeed(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Ime:', required=False)
    surname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Priimek:', required=False)
    enrollment_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Vpisna številka:',
                                    required=False)


class OptionalSubjectsToDataBase(forms.Form):  # forma za izbirne predmete
    modul = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)


class ExamEntryForm(forms.Form):
    now = timezone.now()
    exam_date = forms.DateTimeField(label='Datum:', initial=now, required=True,
                                    widget=forms.TimeInput(format='%Y-%m-%d %H:%M'),
                                    error_messages={'required': 'Manjka datum'}, input_formats=['%Y-%m-%d %H:%M'])
    predmet_queryset = Predmet.objects.all()
    subject = forms.ModelChoiceField(label='Predmet', queryset=predmet_queryset,
                                     error_messages={'required': 'Izberite predmet'})

    def clean(self):
        form_data = self.cleaned_data
        exam_date = form_data['exam_date']
        today = timezone.now()
        if exam_date < today:
            raise forms.ValidationError("Neveljaven datum")
        subject = form_data['subject']
        exams = IzpitniRok.objects.filter(datum=exam_date, predmet=Predmet.objects.get(naziv=subject)).count()
        if exams > 0:
            raise forms.ValidationError("Izpitni rok že obstaja")

        ir = IzpitniRok(idizpitni_rok=IzpitniRok.objects.all().count() + 1, datum=exam_date,
                        predmet=Predmet.objects.get(naziv=subject))
        ir.save()
        return form_data


class ExamTermEditForm(forms.Form):
    def __init__(self, izpitni_rok_in, *args, **kwargs):
        super(ExamTermEditForm, self).__init__(*args, **kwargs)
        izpitni_rok = izpitni_rok_in

        self.fields['old_date'] = forms.DateTimeField(label='Datum:', initial=izpitni_rok.datum, required=True,
                                                      widget=forms.TimeInput(format='%Y-%m-%d %H:%M',
                                                                             attrs={'readonly': 'readonly'}))
        self.fields['new_date'] = forms.DateTimeField(label='Datum:', initial=izpitni_rok.datum, required=True,
                                    widget=forms.TimeInput(format='%Y-%m-%d %H:%M'),
                                    error_messages={'required': 'Manjka datum'}, input_formats=['%Y-%m-%d %H:%M'])

    def clean(self):
        form_data = self.cleaned_data
        old_date = form_data['old_date']
        new_date = form_data['new_date']

        if new_date <= old_date:
            raise forms.ValidationError('Neveljaven datum')