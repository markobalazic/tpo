# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models


class Blocked(models.Model):
    ip = models.CharField(max_length=20, primary_key=True)
    locktime_str = models.CharField(max_length=50, null=True)

    class Meta:
        managed = True
        db_table = 'blocked'


class Sklep(models.Model):
    idsklep = models.IntegerField(primary_key=True)
    besedilo = models.CharField(max_length=256, blank=True)
    vpisna = models.ForeignKey('Student', db_column='vpisna')

    def __unicode__(self):
        return self.besedilo


class Drzava(models.Model):
    iddrzava = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=120, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'drzava'


class IzvedbaPredmeta(models.Model):
    idizvedbe = models.IntegerField(primary_key=True)
    studijsko_leto = models.CharField(max_length=9, db_column='studijsko_leto')
    sifra_profesorja = models.ForeignKey('Profesor', db_column='sifra_profesorja')
    idpredmetnik_stud_programa = models.ForeignKey('PredmetnikStudPrograma', db_column='idpredmetnik_stud_programa')

    def __unicode__(self):
        return self.studijsko_leto + self.sifra_profesorja.ime + self.sifra_profesorja.priimek

    class Meta:
        managed = True
        db_table = 'izvedba_predmeta'


class NacinStudija(models.Model):
    idnacin_studija = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=150, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'nacin_studija'


class Obcina(models.Model):
    idobcina = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'obcina'


class OblikaStudija(models.Model):
    idoblika_studija = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=150, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'oblika_studija'


class Posta(models.Model):
    postna_stevilka = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=256, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'posta'


class Predmet(models.Model):
    sifra_predmeta = models.CharField(primary_key=True, max_length=10)
    naziv = models.CharField(max_length=120, blank=True)
    st_kreditov = models.IntegerField(blank=True, null=True)
    idmodul = models.ForeignKey('Modul', db_column='idmodul')

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'predmet'


class PredmetnikStudPrograma(models.Model):
    idpredmetnik_stud_programa = models.IntegerField(primary_key=True)
    letnik = models.CharField(max_length=45)
    predmet = models.ForeignKey(Predmet, db_column='predmet')
    idstudijski_program = models.ForeignKey('StudijskiProgram', db_column='idstudijski_program')

    def __unicode__(self):
        return self.idpredmetnik_stud_programa

    class Meta:
        managed = True
        db_table = 'predmetnik_stud_programa'


class Profesor(models.Model):
    sifra_profesorja = models.IntegerField(primary_key=True)
    ime = models.CharField(max_length=45, blank=True)
    priimek = models.CharField(max_length=45, blank=True)

    def __unicode__(self):
        return self.ime + self.priimek

    class Meta:
        managed = True
        db_table = 'profesor'


class Student(models.Model):
    vpisna = models.IntegerField(primary_key=True)
    ime = models.CharField(max_length=100)
    priimek = models.CharField(max_length=100)
    naslov = models.CharField(max_length=150, blank=True, null=True)
    spol = models.CharField(max_length=45, blank=True, null=True)
    datum_rojstva = models.DateField(blank=True, null=True)
    obcina = models.ForeignKey(Obcina, db_column='obcina', blank=True, null=True)
    drzavljanstvo = models.ForeignKey(Drzava, db_column='drzavljanstvo', related_name='student_drzava_drzavljanstvo', null=True)
    drzava_rojstva = models.ForeignKey(Drzava, db_column='drzava_rojstva', null=True)
    posta_postna_stevilka = models.ForeignKey(Posta, db_column='posta_postna_stevilka', related_name='student_drzava_rojstva', null=True)
    davcna = models.IntegerField(blank=True, null=True)
    emso = models.CharField(max_length=20, blank=True, null=True)
    telefonska = models.CharField(max_length=150, blank=True, null=True)
    naslov_vrocanja = models.CharField(max_length=45, blank=True, null=True)
    posta_vrocanja = models.ForeignKey(Posta, db_column='posta_vrocanja', null=True)
    email = models.CharField(max_length=200)

    def __unicode__(self):
        return self.vpisna

    class Meta:
        managed = True
        db_table = 'student'


class StudijskiProgram(models.Model):
    idstudijski_program = models.IntegerField(primary_key=True, max_length=7)
    naziv = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'studijski_program'


class Vpis(models.Model):
    stevilka_vpisa = models.IntegerField(primary_key=True)
    studijsko_leto = models.CharField(max_length=9, db_column='studijsko_leto')
    student_vpisna = models.ForeignKey(Student, db_column='student_vpisna')
    idstudijski_program = models.ForeignKey(StudijskiProgram, db_column='idstudijski_program', null=True)
    letnik = models.IntegerField(db_column='letnik')
    idvrsta_vpisa = models.ForeignKey('VrstaVpisa', db_column='idvrsta_vpisa', null=True)
    idnacin_studija = models.ForeignKey(NacinStudija, db_column='idnacin_studija', null=True)
    idoblika_studija = models.ForeignKey(OblikaStudija, db_column='idoblika_studija', null=True)
    koncan_vpis = models.IntegerField(default=0, null=True)
    potrjen_vpis = models.IntegerField(default=0, null=True)
    stevec_potrdil = models.IntegerField(default=1, null=True)

    def __unicode__(self):
        return self.stevilka_vpisa

    class Meta:
        managed = True
        db_table = 'vpis'


class VpisaniPredmeti(models.Model):
    idvpisani_predmeti = models.IntegerField(primary_key=True)
    sifra_predmeta = models.ForeignKey(Predmet, db_column='sifra_predmeta', related_name='vpisani_predmeti_predmet')
    vpis_stevilka_vpisa = models.ForeignKey(Vpis, db_column='vpis_stevilka_vpisa', related_name='vpisani_predmeti_stevilka_vpisa')

    def __unicode__(self):
        return self.idvpisani_predmeti

    class Meta:
        managed = True
        db_table = 'vpisani_predmeti'


class VrstaVpisa(models.Model):
    idvrsta_vpisa = models.IntegerField(primary_key=True)
    naziv = models.CharField(max_length=250, blank=True)

    def __unicode__(self):
        return self.naziv

    class Meta:
        managed = True
        db_table = 'vrsta_vpisa'


class Document(models.Model):
    ime_dokumenta = models.FileField(upload_to='dokumenti_dijakov/')

    def __unicode__(self):
        return self.ime_dokumenta

    class Meta:
        managed = True
        db_table = 'documents'


class User(models.Model):
    iduser = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=200)
    geslo = models.CharField(max_length=40, blank=True)
    role = models.CharField(max_length=200)

    def __unicode__(self):
        return self.email

    class Meta:
        managed = True
        db_table = 'user'


class Modul(models.Model):
    idmodul = models.IntegerField(primary_key=True, max_length=6)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'modul'


class IzpitniRok(models.Model):
    idizpitni_rok = models.IntegerField(primary_key=True)
    datum = models.DateTimeField()
    predmet = models.ForeignKey(Predmet, db_column='predmet', related_name='izpitni_rok_predmet')

    def __unicode__(self):
        return self.predmet.naziv + '//' + self.datum.strftime('%Y-%m-%d %H:%M')


class IzpitniRokPrijava(models.Model):
    idizpitni_rok_prijava = models.IntegerField(primary_key=True)
    izpitni_rok = models.ForeignKey(IzpitniRok, db_column='izpitni_rok', related_name='izpitni_rok_prijave_izpitni_rok')
    student = models.ForeignKey(Student, db_column='student', related_name='izpitni_rok_prijave_student')

    def __unicode__(self):
        return self.student.vpisna + ' : ' + self.izpitni_rok.__unicode__()


class Obvestilo(models.Model):
    idobvestilo = models.IntegerField(primary_key=True)
    student = models.ForeignKey(Student, db_column='student', related_name='obvestilo_student')
    tekst = models.CharField(max_length=512, null=True)
