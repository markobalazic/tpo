# -*- coding: utf-8 -*-
__author__ = 'mnh92'
import time
import string
import random
from estudis.models import Student, User, StudijskiProgram, Vpis, VrstaVpisa, NacinStudija


# generiranje pdf
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape


def create_student(ime, priimek, naslov, spol, datum_rojstva, obcina, drzavljanstvo, drzava_rojstva,
                   posta_postna_stevilka,
                   davcna, emso, telefonska, naslov_vrocenja, posta_vrocenja, email, geslo, fakulteta):
    vpisna = generate_enrollment_no(fakulteta)
    s = Student(vpisna=vpisna, ime=ime, priimek=priimek, naslov=naslov, spol=spol, datum_rojstva=datum_rojstva,
                obcina=obcina,
                drzavljanstvo=drzavljanstvo, drzava_rojstva=drzava_rojstva, posta_postna_stevilka=posta_postna_stevilka,
                davcna=davcna, emso=emso, telefonska=telefonska, naslov_vrocenja=naslov_vrocenja,
                posta_vrocenja=posta_vrocenja,
                email=email, geslo=geslo)

    s.save()


def create_student_pero(ime, priimek, program, email):
    vpisna = generate_enrollment_no(63)  # 63 je koda za FRI
    geslo = ''.join(random.choice(string.ascii_letters) for _ in range(8))
    s = Student(ime=ime, priimek=priimek, email=email, vpisna=vpisna)
    u = User(iduser=User.objects.count() + 1, email=email, geslo=geslo, role='student')
    u.save()
    s.save()
    sp = StudijskiProgram.objects.get(naziv=program)
    vv = VrstaVpisa.objects.get(idvrsta_vpisa=1, naziv='Prvi vpis/dodatni letnik')
    ns = NacinStudija.objects.get(idnacin_studija=1, naziv='redni')
    v = Vpis(stevilka_vpisa=Vpis.objects.count() + 1, studijsko_leto="2014/15", idnacin_studija=ns, student_vpisna=s,
             idstudijski_program=sp, letnik=2, idvrsta_vpisa=vv)
    v.save()
    return s


def create_minimal_student(ime, priimek, program, email):
    vpisna = generate_enrollment_no(63)  # 63 je koda za FRI
    geslo = ''.join(random.choice(string.ascii_letters) for _ in range(8))
    s = Student(ime=ime, priimek=priimek, email=email, vpisna=vpisna)
    u = User(iduser=User.objects.count() + 1, email=email, geslo=geslo, role='student')
    u.save()
    s.save()
    sp = StudijskiProgram.objects.get(naziv=program)
    v = Vpis(stevilka_vpisa=Vpis.objects.count() + 1, student_vpisna=s, idstudijski_program=sp, letnik=1)
    v.save()
    return s


def read_student_info(filename):  # metoda prebere dijake in jih vrne kot studente ,filename = string!
    vnesenih = 0
    duplikati = 0
    with open(filename) as f:
        for line in f:
            line = unicode(line, "utf-8")
            ime = line[0:29].strip()
            priimek = line[29:59].strip()
            program = line[59:66].strip()
            mail = line[66:136].strip()
            if Student.objects.filter(email=mail).count() < 1:
                vnesenih += 1
                s = create_minimal_student(ime, priimek, program, mail)
            else:
                duplikati += 1
    return [vnesenih, duplikati]


def generate_enrollment_no(fakulteta):
    # nikjer v fajlu ne smejo biti sumniki!

    year = time.strftime('%Y')
    faculty_id = str(fakulteta)
    # student_count = Student.objects.filter(vpisna__startswith=faculty_id).count()
    # #TODO: zaenkrat samo za naso fakulteto
    student_count = Student.objects.count()
    student_count += 1
    add = ""
    if student_count < 10:
        add = "000"
    elif student_count < 100:
        add = "00"
    elif student_count < 1000:
        add = "0"
    enrollment_no = faculty_id + year[-2:] + add + str(student_count)
    return int(enrollment_no)


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))


def check_emso(emso):
    emso_factor_map = [7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2]
    sum = 0
    for i in range(12):
        sum += int(emso[i]) * emso_factor_map[i]
    ctrl_no = 11 - (sum % 11)
    if ctrl_no == int(emso[12]):
        return True
    else:
        return False
